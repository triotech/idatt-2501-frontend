# TokenTrivia Frontend Project

Welcome to the frontend repository for development on TokenTrivia as an application.

_Trivia with a twist of Crypto_

![TokenTrivia Logo](public/img/logo.png)

## Table of contents

1. [Description](#description)
2. [Installation](#installation)
3. [Functionalities](#functionalities)
4. [Contributions](#contributions)

## Description

TokenTrivia is an application that explores the possibilities for combining regular web-2 with innovative web-3 technologies. TokenTrivia's porpose as a project is to serve as a functioning application. In addition, the development of TokenTrivia is an experiment to research how new web-3 technologies compares to development using more mature options.

The repository holds the source code for the frontend of TokenTrivia. TokenTrivia is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). This framework enables fast builds and extends the features of the latest React version ([Next.js](https://nextjs.org/) ).

TokenTrivia, being a Next.js project, leverages the advantages of both [server-side rendering (SSR)](https://nextjs.org/docs/pages/building-your-application/rendering/server-side-rendering) and [client-side rendering (CSR)](https://nextjs.org/docs/pages/building-your-application/rendering/client-side-rendering) to offer an optimal user experience and performance.

## Installation

Please follow these steps to setup and start the application frontend of TokenTrivia:

1. Clone the repository:

-   Clone with SSH:

```bash
git clone git@gitlab.stud.idi.ntnu.no:triotech/idatt-2501-frontend.git
```

-   Clone with HTTPS:

```bash
git clone https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-frontend.git
```

2. Install packages

```bash
npm install
```

3. Run the development project locally:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to visit TokenTrivia.

For the best performance, it is recommended by the developers to use [Firefox Developer Edition](https://www.mozilla.org/en-US/firefox/developer/).

## Functionalities

### **Running the app:**

To run the application, please follow the instructions above. This will set up the running frontend of TokenTrivia.

### **Testing:**

Testing is important to ensure that new features does not break any previous implemented functionality. To develop the application in a safe manner, it is recommended to further the implementation of both end-to-end- and component (unit) tests. These are the two types of testing methods utilized originally during development. The testing framework is Cypress [https://www.cypress.io/](https://www.cypress.io/), which allows highly modular testing. The Cypress tests are utilized by the CICD to ensure continuous code quality and functionality. Cypress is important to create reliable code. Therefore, the frontend code relies on both Cypress component tests that validates the component unit parts, and more interactivity-dependent end-to-end-tests of the pages.

#### Testing the frontend using Cypress:

##### _Running component tests in terminal: (alternative 1)_

-   1. Open a terminal and type:

```bash
npm run cy:comp
```

##### _Running component tests in Cypress UI-environment: (alternative 2)_

-   1. Open a terminal and type:

```bash
npm run cy:open
```

or

```bash
npx cypress open
```

and choose component testing.

-   2. Follow the provided instructions in the Cypress-UI to manually run and view the component tests.

##### _Running e2e-tests in terminal: (alternative 1)_

-   1. Open the backend repo [https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend](https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend) and follow the instructions to _start_ the server.

-   2. Open terminal 1 and type:

```bash
npm run dev
```

-   3. Open terminal 2 and type:

```bash
npm run cy:run
```

or

```bash
npm run cy:headless
```

##### _Running e2e-tests in Cypress UI-environment: (alternative 2)_

-   1. Open the backend repo [https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend](https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend) and follow the instructions to _start_ the server.

-   2. Open terminal 1 and type:

```bash
npm run dev
```

-   3. Open terminal 2 and type:

```bash
npm run cy:open
```

or

```bash
npx cypress open
```

and choose end-to-end testing.

-   4. Follow the provided instructions in the Cypress-UI to manually run and view the end-to-end tests.

### **Continous integration and continous delivery (CICD):**

The CICD pipeline is in place to ensure continous verification and deployment of the code. The pipeline is structured in distinct stages with assigned jobs. There are four essential steps which might be triggered on code changes; install, build, test and deploy.

Stages:

-   1. _Install:_ ensures that the dependencies can be installed by using an CI-adapted command 'npm ci'.

-   2. _Build:_ enforces the 'npm run build' command that checks if the next app can in fact be built. This step fails if there are any build-errors.

-   3. _Test:_ validates the code by using Cypress component- and e2e-testing. This ensures that the core functionality of the applcation has not been tampered with by any new changes. This step consists of two parallell jobs; component_test and e2e_headless - described by their respective names.

The component test job runs the cypress component tests. It logs the output from the terminal to a dedicated .log-file.

The e2e test job depends on the before_script defined by the \_setup_ssh YAML anchor. This before_script ensures that the CI-environment has correct access rights to the repo by adding the masked SSH private key (SSH_PK) env variable to the CI-environment and also adds gitlab.stud.idi.ntnu.no to known hosts.

The e2e then uses the added ssh to clone the backend and run that in parallell while starting the cypress e2e tests in a headless browser. The job logs the output from the terminal to a .log-file when finished. This can be downloaded as an artifact from the gitlab project.

-   4. _Deploy:_ ensures that the built frontend is pushed to https://JFBendvold:$GITHUB_TOKEN@github.com/JFBendvold/JFBendvold.github.io.git (Github Pages). This step is only enforced on merge requests to the branch 'deploy'.

### **Usage of components and libraries:**

TokenTrivia leverages several key components and libraries to enhance its functionality and user experience. Here is an overview of some of the main dependencies and their roles in the application:

-   @fingerprintjs/fingerprintjs (v4.1.0): This library is utilized for device fingerprinting, which helps in uniquely identifying users' devices. It's mainly used for sequrity and integrity.

-   antd (v5.10.1): Ant Design provides React UI components that are used throughout TokenTrivia to create a consistent and user-friendly interface.

-   axios (v1.5.1): Axios is crucial for making HTTP requests to external services and APIs.

-   react-confetti (v6.1.0): This library adds a fun and visually appealing confetti effect, which is used in-game.

-   react-rewards (v2.0.4): React Rewards is similar to react-confetti and improves the in-game experience with different effects.

-   react-select (v5.7.7): This component provides a flexible and customizable select input.

-   react-tooltip (v5.21.5): This library offers tooltips to users in a concise format, improving the overall usability and user experience.

-   socket.io-client (v4.7.2): Used for real-time event-based communication. It plays a key role in enabling live interactions and updates in the game.

-   web3 (v4.1.2): This is a collection of libraries that allow you to interact with a local or remote Ethereum node. It's essential for the integration of web-3 in TokenTrivia, enabling features like cryptocurrency transactions.

## Contribution

To view the backend repository, please visit: [https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend](https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend)

To contribute to the of our frontend project, please follow these steps:

1. Before making any changes, create a new issue in the [backend repo](https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend). Make sure that the issue describes the contribution you plan to make. This will help to ensure that your changes align with the project goals and development aims.
2. Once the issue is approved, create a new branch for the issue, using a descriptive name that summarizes the changes you plan to make.
3. When you have completed your changes, push the branch to our frontend Git repository. This will trigger our pipeline to check your changes for any issues or errors.
4. If the pipeline check is successful, submit a merge request to the main branch. Please include a detailed description of your changes and any relevant information that may help us review your request.
5. After your changes have been merged, close the issue and delete the branch from the repository. This helps to keep our repository organized and maintainable.
