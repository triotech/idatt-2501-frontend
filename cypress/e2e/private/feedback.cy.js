// Logs the cypresstest user in by mocking the login process
const loginUser = () => {
    const name = "cypresstest";
    cy.session(name, () => {
        window.localStorage.setItem(
            "user",
            JSON.stringify({
                name: name,
                email: "cypress@test.com",
                country: "NO",
                timestamp: "2023-10-26T09:36:11.220Z",
                avatar_url: "images/1698312971220-48a79014.png",
                tokens: 0,
            }),
        );
    });
    cy.request("POST", "http://localhost:8080/auth/authenticate", {
        name: "cypresstest",
        password: "cypress123",
        fingerprint: 123,
    }).then((response) => {
        cy.request("POST", "http://localhost:8080/auth/set-cookie", {
            token: response.body.jwt_token,
        });
        cy.wait(200);
    });
};

//Mocks logout for the tests by manually clearing the local storage
const logout = () => {
    window.localStorage.clear();
};

describe("visits the feedback page", () => {
    beforeEach(() => {
        loginUser();
    });

    it("visits url and renders page", () => {
        cy.visit("http://localhost:3000/feedback");
        cy.contains("Feedback");
    });
});

describe("functionality for the feedback page", () => {
    beforeEach(() => {
        loginUser();
        cy.visit("http://localhost:3000/feedback");
    });

    afterEach(() => {
        logout();
    });

    // it('attempts to send feedback', () => { //Session needs to be cleared repeadedly for this to work
    //     cy.intercept({
    //         method: 'POST',
    //         url: '**/feedback/add',
    //     }).as('sendFeedback')
    //     cy.get('textarea').type('This is a test feedback');
    //     cy.get('button').contains('Submit').click()
    //     cy.wait('@sendFeedback').then((interception) => {
    //       assert.isNotNull(interception.request, 'POST request was made');
    //   });
    // })

    it("attempts to send feedback without a message", () => {
        cy.get("button").contains("Submit").click();
        cy.contains("Please enter some feedback");
    });
});
