// Logs the cypresstest user in by mocking the login process
const loginUser = () => {
    const name = "cypresstest";
    cy.session(name, () => {
        window.localStorage.setItem(
            "user",
            JSON.stringify({
                name: name,
                email: "cypress@test.com",
                country: "NO",
                timestamp: "2023-10-26T09:36:11.220Z",
                avatar_url: "images/1698312971220-48a79014.png",
                tokens: 0,
            }),
        );
    });
    cy.request("POST", "http://localhost:8080/auth/authenticate", {
        name: "cypresstest",
        password: "cypress123",
        fingerprint: 123,
    }).then((response) => {
        cy.request("POST", "http://localhost:8080/auth/set-cookie", {
            token: response.body.jwt_token,
        });
        cy.wait(200);
    });
};

//Mocks logout for the tests by manually clearing the local storage
const logout = () => {
    window.localStorage.clear();
};

describe("visits the logout page", () => {
    beforeEach(() => {
        loginUser();
    });

    it("visits url and renders page", () => {
        cy.visit("http://localhost:3000/logout");
        cy.contains("Are you sure you want to log out?");
    });
});

describe("functionality for the logout page", () => {
    beforeEach(() => {
        loginUser();
        cy.visit("http://localhost:3000/logout");
    });

    it("logs the user out by pressing the log out button", () => {
        cy.get("button").contains("Log out").click();
        cy.wait(2000);
        cy.contains("Login");
    });
});
