// Tests that the user can visit the home page
describe("visits the home page", () => {
    it("visits url and renders page", () => {
        cy.visit("http://localhost:3000/");
        cy.contains("Trivia with a twist of Crypto");
    });
});

// Tests to verify that the functionality of the home page works as intended
describe("tests out the functionality for the home page", () => {
    //Ensures that the home page is visited before each test
    beforeEach(() => {
        cy.visit("http://localhost:3000/");
    });

    // Tests that the user can visit the login page by clicking the login button
    it("clicks on the Login button", () => {
        cy.get("button").contains("Login").click();
        cy.get("body").should("not.contain", "Trivia with a twist of Crypto");
        cy.contains("Forgot Password");
    });

    // Tests that the user can visit the register page by clicking the register button
    it("clicks on the Register button", () => {
        cy.get("button").contains("Register").click();
        cy.get("body").should("not.contain", "Trivia with a twist of Crypto");
        cy.contains("Terms of Service");
    });

    // Tests that the user can visit the cookie page by clicking the about button
    it("clicks on the Cookie icon", () => {
        cy.get("a").contains("cookie").click();
        cy.contains("This is the Cookie Policy for TokenTrivia, accessible from this link.");
    });
});
