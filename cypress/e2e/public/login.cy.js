describe("visits the home page", () => {
    it("visits url and renders page", () => {
        cy.visit("http://localhost:3000/login");
        cy.contains("Forgot Password");
    });
});

// Tests to verify that the functionality of the login page works as intended
describe("tests out the functionality for the login page", () => {
    //Ensures that the login page is visited before each test
    beforeEach(() => {
        cy.visit("http://localhost:3000/login");
    });

    // Tests that the user receives correct feedback with no input
    it("clicks on the Login button without any input", () => {
        cy.get("button").contains("Login").click();
        cy.contains("Please enter a username or email");
        cy.contains("No username");
    });

    // Tests that the user receives correct feedback with only a username inputted
    it("clicks on the Login button with only inputted username", () => {
        cy.get('input[placeholder="Username/Email"]').type("testtesttest");
        cy.get("button").contains("Login").click();
        cy.contains("Please enter a password");
        cy.contains("No password");
    });

    // Tests that the user receives correct feedback with only a password inputted
    it("clicks on the Login button with only inputted password", () => {
        cy.get('input[placeholder="Password"]').type("testtesttest");
        cy.get("button").contains("Login").click();
        cy.contains("Please enter a username or email");
        cy.contains("No username");
    });

    // Tests that the user receives correct feedback with correct inputs
    // ----> commented out due to impracticality of testing repeatedly with a real database
    // it('clicks on the Login button with correct inputs', () => {
    //     cy.get('input[placeholder="Username/Email"]').type('cypresstester');
    //     cy.get('input[placeholder="Password"]').type('password12345');
    //     cy.get('button').contains('Login').click()
    //     cy.get('body').should('not.contain', 'Forgot Password')
    //     cy.contains('Join a game')
    //     cy.contains('Find a game')
    // })

    // Tests that the user can visit the register page by clicking the register button
    it("clicks on the Register button", () => {
        cy.get("a").contains("Register").click();
        cy.get("body").should("not.contain", "Forgot Password");
        cy.contains("Terms of Service");
    });
});
