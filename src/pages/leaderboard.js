import { useState, useEffect, useRef, useLayoutEffect } from "react";
import styles from "@/style/leaderboard.module.css";
import Header from "@/components/header/Header";
import Image from "next/image";
import LoadingScreen from "@/components/LoadingScreen";
import { useRouter } from "next/router";
import { getScoreTable } from "@/services/UserService";
import { localUrl } from "@/utils/ApiClientSetup";
import { showLoadingScreen, hideLoadingScreen } from "@/utils/LoadingProvider";

export default function Leaderboard() {
    const [user, setUser] = useState({});
    const [style, setStyle] = useState("#FF70A6");
    const [input, setInput] = useState("");
    const [selected, setSelected] = useState("Bug");
    const router = useRouter();
    const [leaderboardData, setLeaderboardData] = useState([]);

    const mainRef = useRef(null);

    useLayoutEffect(() => {
        showLoadingScreen();
        // Get user from local storage
        const initialUser = localStorage.getItem("user"); // Get user from local storage

        if (initialUser) {
            // If user is found in local storage
            setUser(JSON.parse(initialUser));
        } else {
            console.error("No user found in local storage");
        }

        // Get leaderboard data
        getScoreTable().then((response) => {
            if (response.status === 200) {
                setLeaderboardData(response.data);
            } else {
                console.error("Error fetching leaderboard data");
            }

            hideLoadingScreen();
        });
    }, []);

    // Set style
    useEffect(() => {
        if (user && user.style) {
            setStyle(user.style);

            // Set background color
            document.body.style.background = `${user.style}`;
        }
    }, [user]);

    return (
        <main className={styles.main} ref={mainRef}>
            <Header />
            <div className={styles.content}>
                <h1 className={styles.title}>Leaderboard</h1>
                <div className={styles.leaderboard}>
                    {leaderboardData.map((user, index) => {
                        return (
                            <div
                                className={styles.user}
                                key={index}
                                style={{
                                    backgroundColor:
                                        index === 0
                                            ? "rgba(218, 165, 32, 0.7)"
                                            : index === 1
                                            ? "rgba(192, 192, 192, 0.3)"
                                            : index === 2
                                            ? "rgba(218, 165, 32, 0.3)"
                                            : "background-color: rgba(0, 0, 0, 0.2)",
                                }}
                            >
                                <div className={styles.position}>{index + 1}.</div>
                                <div className={styles.username}>
                                    <Image
                                        src={`${localUrl}/users/image/${user.name}`}
                                        alt="Profile Picture"
                                        width={50}
                                        height={50}
                                        className={styles.avatar}
                                    />
                                    {user.name}
                                    <Image
                                        src={"../flags/" + user.country + ".svg"}
                                        alt="Country"
                                        width={24}
                                        height={24}
                                        className={styles.flag}
                                    />
                                </div>
                                <div className={styles.score}>{user.score} wins</div>
                            </div>
                        );
                    })}
                </div>
            </div>
            <LoadingScreen />
        </main>
    );
}
