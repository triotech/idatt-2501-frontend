import { useState, useEffect, useLayoutEffect } from "react";
import styles from "../../style/settings/settingsContent.module.css";
import { Tooltip } from "react-tooltip";
import LoadingScreen from "@/components/LoadingScreen";
import {
    openNotificationError,
    openNotificationSuccess,
    openNotificationInfo,
} from "@/utils/Notifications.js";
import { getGameHistory } from "@/utils/Web3Connection";

export default function HistorySettings(user) {
    const [gameHistory, setGameHistory] = useState([]);

    useEffect(() => {
        // Get game history
        getGameHistory().then((gameHistory) => {
            // Create a copy of the array and sort it
            let sortedGameHistory = [...gameHistory].sort((a, b) => {
                return Number(b[1]) - Number(a[1]);
            });

            setGameHistory(sortedGameHistory);
        });
    }, []);

    function convertUNIX(unix) {
        let date = new Date(unix);

        return date.toLocaleString();
    }

    return (
        <div className={styles.container}>
            <div className={styles.profile}>
                <h2>Game History</h2>
            </div>
            <div className={styles.box} style={{ overflowY: "scroll" }}>
                {gameHistory?.map((game, index) => {
                    let buyIn = Number(game[0]) * 10 ** -14;
                    let lastQuestion = Number(game[3]);
                    let numOfPlayers = Number(game[2]);
                    let time = convertUNIX(Number(game[1]));

                    return (
                        <div className={styles.gameBox} key={index}>
                            <div className={styles.gameInfo}>
                                <div className={styles.gamePrice}>
                                    <p>{buyIn} Tokens</p>
                                </div>
                                <div className={styles.score}>
                                    <p>{lastQuestion}/10</p>
                                </div>
                            </div>
                            <div className={styles.gameDate}>
                                <p>{time}</p>
                            </div>
                            <div className={styles.players}>
                                <p>{numOfPlayers} Players</p>
                            </div>
                        </div>
                    );
                })}
                {gameHistory.length == 0 && <p className={styles.noGames}>No games played yet</p>}
            </div>
            <LoadingScreen />
            <Tooltip id="tooltip" />
        </div>
    );
}
